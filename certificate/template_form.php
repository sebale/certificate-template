<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/mod/certificate/lib.php');

class mod_certificate_create_template_field_img_form extends moodleform {
	    function definition() {
        global $CFG,$template_id;
		
		$mform =& $this->_form;
				
		$mform->addElement('text', 'field_name', get_string('fieldname','certificate'));
		$mform->addRule('field_name', null, 'required', null, 'client');
		$mform->setType('field_name', PARAM_TEXT);
		
		$mform->addElement('filepicker', 'image', get_string('image','certificate'), null, array( 'accepted_types' => array('jpe' => 'image/jpeg',
                                'jpeIE' => 'image/pjpeg',
                                'jpeg' => 'image/jpeg',
                                'jpegIE' => 'image/pjpeg',
                                'jpg' => 'image/jpeg',
                                'jpgIE' => 'image/pjpeg',
                                'png' => 'image/png',
                                'pngIE' => 'image/x-png'))); 
								
		$mform->addElement('text', 'z', get_string('z-index','certificate'));
		$mform->setType('z', PARAM_INT);		
		$mform->addRule('z', null, 'required', null, 'client');
		
		$mform->addElement('text', 'x', get_string('positionx','certificate'));
		$mform->setType('x', PARAM_INT);
		$mform->addElement('text','y', get_string('positiony','certificate'));
		$mform->setType('y', PARAM_INT);
		$mform->addElement('text', 'w', get_string('width','certificate'));
		$mform->setType('w', PARAM_INT);
		$mform->addElement('text','h', get_string('height','certificate'));
		$mform->setType('h', PARAM_FLOAT);
		$mform->addElement('text', 'alpha', get_string('alpha','certificate'));
		$mform->setType('alpha', PARAM_INT);
		$mform->setDefault('alpha', "100");
		
		$mform->addElement('hidden','field_type', 'field_type');
		$mform->setType('field_type', PARAM_RAW);
		$mform->setDefault('field_type', "img");
	
		$mform->addElement('hidden', 'template_id', 'template_id');
		$mform->setType('template_id', PARAM_RAW);
		$mform->setDefault('template_id', $template_id);

		
		$this->add_action_buttons();
    }
    /**
     * Some validation - Michael Avelar <michaela@moodlerooms.com>
     */
    function validation($data, $files) {
                return ;
    }
}
class mod_certificate_create_template_field_text_form extends moodleform {
	
    function definition() {
        global $CFG,$template_id;
		
				$text_types = array(
					"date" => get_string('date'),
					"grade" => get_string('grade'),
					"teacher" => get_string('teacher','certificate'),
					"user_lastname" => get_string('userlastname','certificate'),
					"user_firstname" => get_string('userfirstname','certificate'),
					"user_name" => get_string('username','certificate'),
					"user_idnumber" => get_string('user_idnumber','certificate'),
					"course_name" => get_string('coursename','certificate'),
					"code" => get_string('code','certificate'),
					"certname" => get_string('certificatename','certificate'),
					"lesson_list" => get_string('lesson_list','certificate'),
					"custom_text" => get_string('customtext','certificate')
				);
				
				$text_leveling = array(
					"L" => get_string('left','certificate'),
					"C" => get_string('center','certificate'),
					"R" => get_string('right','certificate')
				);
				$font_style = array(
					"" => "Normal",
					"B" => "Bold",
					"U" => "Underline",
					"D" => "Line Through",
					"O" => "Overline",
					"I" => "Italic"
				);
				$text_fonts = array(
					"freeserif" => "Free Serif",
					"times" => "Times-Roman",
					"helvetica" => "Helvetica",
					"courier" => "Courier",
					"symbol" => "Symbol",
					"zapfdingbats" => "ZapfDingbats",
					"freesans" => "Free Sans"
				);
				$mform =& $this->_form;
				$mform->addElement('text', 'field_name', get_string('fieldname','certificate'));
				$mform->addRule('field_name', null, 'required', null, 'client');
				$mform->setType('field_name', PARAM_TEXT);
				
				$mform->addElement('text', 'z', get_string('z-index','certificate'));
				$mform->setType('z', PARAM_INT);		
				$mform->addRule('z', null, 'required', null, 'client');
		
				$mform->addElement('select', 'text_types', get_string('texttypes','certificate'), $text_types,array("onmouseup"=>"schow_text_input();"));
				$mform->addElement('html', '<div id="text_area" style="display:none;">');
				$mform->addElement('textarea', 'text_area', get_string('customtext','certificate'));
				$mform->setType('text_area', PARAM_RAW);
				$mform->addElement('html', '</div>');
				$mform->addElement('select', 'text_leveling', get_string('textalign','certificate'), $text_leveling);
				$mform->addElement('select', 'text_font', get_string('textfont','certificate'), $text_fonts);
				$mform->addElement('select', 'font_style', get_string('fontstyle','certificate'), $font_style);
				$mform->addElement('text', 'font_size', get_string('fontsize','certificate'));
				$mform->setType('font_size', PARAM_INT);
				$mform->addElement('text', 'font_color', get_string('fontcolor','certificate'), array("class"=>"colorpicker"));
				$mform->setType('font_color', PARAM_RAW);
				$mform->addElement('text', 'x', get_string('positionx','certificate'));
				$mform->setType('x', PARAM_INT);
				$mform->addElement('text', 'y', get_string('positiony','certificate'));
				$mform->setType('y', PARAM_INT);
				$mform->addElement('text', 'w', get_string('width','certificate'));
				$mform->setType('w', PARAM_INT);
				$mform->addElement('text', 'rotate', get_string('rotate','certificate'));
				$mform->setType('rotate', PARAM_INT);
				$mform->addElement('text', 'alpha', get_string('alpha','certificate'));
				$mform->setType('alpha', PARAM_INT);
				$mform->setDefault('alpha', "100");
				
				$mform->addElement('hidden', 'field_type', 'field_type');
				$mform->setType('field_type', PARAM_RAW);
				$mform->setDefault('field_type', "text");
				$mform->addElement('hidden', 'template_id', 'template_id');
				$mform->setType('template_id', PARAM_RAW);
				$mform->setDefault('template_id', $template_id);
						
				$this->add_action_buttons();
    }
	
    function validation($data, $files) {
                return ;
    }
}

class mod_certificate_create_template_form extends moodleform {
	    
		function definition() {
        global $CFG;
		
        $mform =& $this->_form;
		
		$mform->addElement('text', 'template_name', get_string('templatename','certificate'));
		$mform->addRule('template_name', null, 'required', null, 'client');
		$mform->setType('template_name', PARAM_TEXT);
		
		$orientation = array( 'L' => get_string('landscape', 'certificate'), 'P' => get_string('portrait', 'certificate'));
        $mform->addElement('select', 'orientation', get_string('orientation', 'certificate'), $orientation);
        $mform->setDefault('orientation', 'landscape');
        $mform->addHelpButton('orientation', 'orientation', 'certificate');
		
        $this->add_action_buttons();
    }
    /**
     * Some validation - Michael Avelar <michaela@moodlerooms.com>
     */
    function validation($data, $files) {
                return ;
    }
}
class mod_certificate_edit_template_form extends moodleform {
	    
		function definition() {
        global $CFG,$template_id,$DB;
		
		$template = $DB->get_record_sql("SELECT * FROM {certificate_template} WHERE id='$template_id' ");
        $mform =& $this->_form;
		
		$mform->addElement('text', 'template_name', get_string('templatename','certificate'));
		$mform->addRule('template_name', null, 'required', null, 'client');
		$mform->setType('template_name', PARAM_TEXT);
		  $mform->setDefault('template_name', $template->template_name);
		
		$orientation = array( 'L' => get_string('landscape', 'certificate'), 'P' => get_string('portrait', 'certificate'));
        $mform->addElement('select', 'orientation', get_string('orientation', 'certificate'), $orientation);
        $mform->setDefault('orientation', $template->orientation);
        $mform->addHelpButton('orientation', 'orientation', 'certificate');
		
		 $mform->addElement('hidden', 'id', 'id');
		$mform->setType('id', PARAM_RAW);
		$mform->setDefault('id', $template_id); 
		
        $this->add_action_buttons();
    }
    /**
     * Some validation - Michael Avelar <michaela@moodlerooms.com>
     */
    function validation($data, $files) {
                return ;
    }
}
class mod_certificate_edit_template_field_img_form extends moodleform {
	function definition() {
        global $CFG,$field_param;
				
		$value = unserialize($field_param->value);
		
		$mform =& $this->_form;
				
		$mform->addElement('text', 'field_name', get_string('fieldname','certificate'));
		$mform->addRule('field_name', null, 'required', null, 'client');
		$mform->setType('field_name', PARAM_TEXT);
		
		$mform->addElement('filepicker', 'image', get_string('image','certificate'), null, array( 'accepted_types' => array('jpe' => 'image/jpeg',
                                'jpeIE' => 'image/pjpeg',
                                'jpeg' => 'image/jpeg',
                                'jpegIE' => 'image/pjpeg',
                                'jpg' => 'image/jpeg',
                                'jpgIE' => 'image/pjpeg',
                                'png' => 'image/png',
                                'pngIE' => 'image/x-png'))); 
					
		$mform->addElement('text', 'z', get_string('z-index','certificate'));
		$mform->setType('z', PARAM_INT);		
		$mform->addRule('z', null, 'required', null, 'client');
		$mform->addElement('text', 'x', get_string('positionx','certificate'));
		$mform->setType('x', PARAM_INT);
		$mform->addElement('text','y', get_string('positiony','certificate'));
		$mform->setType('y', PARAM_INT);
		$mform->addElement('text', 'w', get_string('width','certificate'));
		$mform->setType('w', PARAM_INT);
		$mform->addElement('text','h', get_string('height','certificate'));
		$mform->setType('h', PARAM_FLOAT);
		$mform->addElement('text', 'alpha', get_string('alpha','certificate'));
		$mform->setType('alpha', PARAM_INT);
		$mform->setDefault('alpha', "100");
		
		$mform->addElement('hidden','field_type', 'field_type');
		$mform->setType('field_type', PARAM_RAW);
		$mform->setDefault('field_type', "img");
	
		$mform->addElement('hidden', 'id', 'id');
		$mform->setType('id', PARAM_RAW);
		$mform->setDefault('id', $field_param->id);
		
			$mform->setDefault('field_name', $field_param->name);
			$mform->setDefault('z', $field_param->z_index);
			$mform->setDefault('image', $value->image);
			$mform->setDefault('h', $value->h);
			$mform->setDefault('w', $value->w);
			$mform->setDefault('y', $value->y);
			$mform->setDefault('x', $value->x);
			$mform->setDefault('alpha', $value->alpha);

		
		$this->add_action_buttons();
    }
    /**
     * Some validation - Michael Avelar <michaela@moodlerooms.com>
     */
    function validation($data, $files) {
                return ;
    }
}
class mod_certificate_edit_template_field_text_form extends moodleform {
		
    function definition() {
        global $CFG,$field_param;
				
				$value = unserialize($field_param->value);
				
				$text_types = array(
					"date" => get_string('date'),
					"grade" => get_string('grade'),
					"teacher" => get_string('teacher','certificate'),
					"user_lastname" => get_string('userlastname','certificate'),
					"user_firstname" => get_string('userfirstname','certificate'),
					"user_name" => get_string('username','certificate'),
                    "user_idnumber" => get_string('user_idnumber','certificate'),
					"course_name" => get_string('coursename','certificate'),
					"code" => get_string('code','certificate'),
					"certname" => get_string('certificatename','certificate'),
					"lesson_list" => get_string('lesson_list','certificate'),
					"custom_text" => get_string('customtext','certificate')
				);
				
				$text_leveling = array(
					"L" => get_string('left','certificate'),
					"C" => get_string('center','certificate'),
					"R" => get_string('right','certificate')
				);
				$font_style = array(
					"" => "Normal",
					"B" => "Bold",
					"U" => "Underline",
					"D" => "Line Through",
					"O" => "Overline",
					"I" => "Italic"
				);
				$text_fonts = array(
					"freeserif" => "Free Serif",
					"times" => "Times-Roman",
					"helvetica" => "Helvetica",
					"courier" => "Courier",
					"symbol" => "Symbol",
					"zapfdingbats" => "ZapfDingbats",
					"freesans" => "Free Sans"
				);
				$mform =& $this->_form;
				$mform->addElement('text', 'field_name', get_string('fieldname','certificate'));
				$mform->addRule('field_name', null, 'required', null, 'client');
				$mform->setType('field_name', PARAM_TEXT);
				
				$mform->addElement('text', 'z', get_string('z-index','certificate'));
				$mform->setType('z', PARAM_INT);		
				$mform->addRule('z', null, 'required', null, 'client');
		
				$mform->addElement('select', 'text_types', get_string('texttypes','certificate'), $text_types,array("onmouseup"=>"schow_text_input();"));
				$mform->addElement('html', '<div id="text_area" style="display:none;">');
				$mform->addElement('textarea', 'text_area', get_string('customtext','certificate'));
				$mform->setType('text_area', PARAM_RAW);
				$mform->addElement('html', '</div>');
				$mform->addElement('select', 'text_leveling', get_string('textalign','certificate'), $text_leveling);
				$mform->addElement('select', 'text_font', get_string('textfont','certificate'), $text_fonts);
				$mform->addElement('select', 'font_style', get_string('fontstyle','certificate'), $font_style);
				$mform->addElement('text', 'font_size', get_string('fontsize','certificate'));
				$mform->setType('font_size', PARAM_INT);
				$mform->addElement('text', 'font_color', get_string('fontcolor','certificate'), array("class"=>"colorpicker"));
				$mform->setType('font_color', PARAM_RAW);
				$mform->addElement('text', 'x', get_string('positionx','certificate'));
				$mform->setType('x', PARAM_INT);
				$mform->addElement('text', 'y', get_string('positiony','certificate'));
				$mform->setType('y', PARAM_INT);
				$mform->addElement('text', 'w', get_string('width','certificate'));
				$mform->setType('w', PARAM_INT);
				$mform->addElement('text', 'rotate', get_string('rotate','certificate'));
				$mform->setType('rotate', PARAM_INT);
				$mform->addElement('text', 'alpha', get_string('alpha','certificate'));
				$mform->setType('alpha', PARAM_INT);
				$mform->setDefault('alpha', "100");
				
				$mform->addElement('hidden', 'field_type', 'field_type');
				$mform->setType('field_type', PARAM_RAW);
				$mform->setDefault('field_type', "text");
				
				$mform->addElement('hidden', 'id', 'id');
				$mform->setType('id', PARAM_INT);
				$mform->setDefault('id', $field_param->id);
				
					$mform->setDefault('field_name', $field_param->name);
					$mform->setDefault('z', $field_param->z_index);
					$mform->setDefault('font_color', $value->font_color);
					$mform->setDefault('w', $value->w);
					$mform->setDefault('y', $value->y);
					$mform->setDefault('x', $value->x);
					$mform->setDefault('alpha', $value->alpha);
					$mform->setDefault('rotate', $value->rotate);
					$mform->setDefault('font_size', $value->font_size);
					$mform->setDefault('font_style', $value->font_style);
					$mform->setDefault('text_font', $value->text_font);
					$mform->setDefault('text_leveling', $value->text_leveling);
					$mform->setDefault('text_area', $value->text_area);
					$mform->setDefault('text_types', $value->text_types);
						
				$this->add_action_buttons();
    }
	
    function validation($data, $files) {
                return ;
    }
}  
