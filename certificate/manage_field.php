<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    mod
 * @subpackage certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

require_login();

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$title = get_string('fields', 'certificate');

$PAGE->set_url('/mod/certificate/manage_field.php');
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($SITE->fullname);
$PAGE->navbar->add('Certificate',new moodle_url("/admin/settings.php", array('section' => "modsettingcertificate")));
$PAGE->navbar->add($title);

$template_id = required_param('id', PARAM_INT);  

			$action = optional_param('action', '', PARAM_ALPHA);
			$field_id = optional_param('field_id', '',PARAM_INT);
			if($action = "delete"){
				$DB->delete_records("certificate_template_field", array("id"=>$field_id));
			}
			
$fields = $DB->get_records_sql("SELECT id,z_index,name,type FROM {certificate_template_field} WHERE template_id='$template_id' ");

$table = new html_table();
$table->head = array('id', get_string('z-index','certificate'), 'Field Name','Field Type', 'Actions');
if(count($fields)<1)$table->data = array(array( "","No Data","")); else $table->data = array();

foreach($fields as $field){
	$link = html_writer::link(new moodle_url($PAGE->url, array('field_id' => $field->id, 'action' => 'delete', 'id' => $template_id)), get_string('delete'));
	$link .=" ";
	if($field->type == "text"){
		$link .= html_writer::link(new moodle_url("/mod/certificate/edit_text.php", array('id' => $field->id)), get_string('edit'));
	}elseif($field->type == "img"){
		$link .= html_writer::link(new moodle_url("/mod/certificate/edit_img.php", array('id' => $field->id)), get_string('edit'));
	}
	
	array_push($table->data,array($field->id, $field->z_index,$field->name,$field->type,$link));
	
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::table($table);
echo html_writer::link(new moodle_url("/mod/certificate/create_img.php", array("id" => $template_id)),get_string('create_field_img', 'certificate'), array('class' => 'button'));
echo html_writer::link(new moodle_url("/mod/certificate/create_text.php", array("id" => $template_id)),get_string('create_field_text', 'certificate'), array('class' => 'button'));
echo $OUTPUT->footer();
?>
