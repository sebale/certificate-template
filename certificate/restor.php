<?php
require_once('../../config.php');
require_once("$CFG->dirroot/mod/certificate/deprecatedlib.php");
require_once("$CFG->dirroot/mod/certificate/lib.php");
exit;
error_reporting(E_ALL);
ini_set('display_errors', '1');

$CFG->debug = 38911; 
$CFG->debugdisplay = true;

$certificates = $DB->get_records_sql("SELECT * FROM {certificate_temp} WHERE courseid > 0 ");

foreach($certificates as $cx){
	$certificate = $DB->get_record('certificate', array('course'=> $cx->courseid));
	
	$data = $DB->get_record('certificate_issues', array('certificateid'=> $certificate->id, 'userid'=> $cx->userid));
	
	if($data->id){
		$data->timecreated =  $cx->timecreated;
		$DB->update_record('certificate_issues', $data);
		
		print("<br>updated - " . $data->id);
	}else{
		$certissue = new stdClass();
		$certissue->certificateid = $certificate->id;
		$certissue->userid = $cx->userid;
		$certissue->code = certificate_generate_code();
		$certissue->timecreated =  $cx->timecreated;
		$certissue->id = $DB->insert_record('certificate_issues', $certissue);
		
		print("<br>inserted - " . $certissue->id);
		
	}
}
die("*");