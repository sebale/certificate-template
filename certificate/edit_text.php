<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    mod
 * @subpackage certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/certificate/lib.php');
require_once($CFG->dirroot.'/mod/certificate/template_form.php');

require_login();

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$field_id = optional_param('id', 0, PARAM_INT);
$template_id = optional_param('template_id', 0, PARAM_INT);
$title = get_string('edit_text', 'certificate');

$PAGE->set_url('/mod/certificate/edit_text.php');
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($SITE->fullname);

if (!$field_param = $DB->get_record('certificate_template_field', array('id'=> $field_id))) {
			print_error('template is misconfigured');
		}
		
$upload_form = new mod_certificate_edit_template_field_text_form($CFG->wwwroot.'/mod/certificate/edit_text.php?template_id='.$field_param->template_id);

if ($upload_form->is_cancelled()) {
    redirect(new moodle_url('/mod/certificate/manage_field.php', array("id" => $template_id)));
} else if ($data = $upload_form->get_data()) {
	$url = new moodle_url('/mod/certificate/manage_field.php', array("id" =>$field_param->template_id));
	$insert = new stdclass;
	$insert->id = $data->id;
	$insert->name = $data->field_name;
	$insert->type = $data->field_type;
	$insert->z_index = $data->z;
	unset ($data->z);
	unset ($data->id);
	unset ($data->field_name);
	unset ($data->field_type);
	unset ($data->submitbutton);
	$insert->value = serialize($data);
	$insert->timecreate = time();
	
	$DB->update_record('certificate_template_field', $insert);
	redirect($url); 
}

$PAGE->navbar->add('Certificate',new moodle_url("/admin/settings.php", array('section' => "modsettingcertificate")));
$PAGE->navbar->add(get_string('fields', 'certificate'),new moodle_url("/mod/certificate/manage_field.php", array('id' => $field_param->template_id)));
$PAGE->navbar->add($title);

echo $OUTPUT->header();
require_once('jq_ui/head.php');
echo $OUTPUT->heading($title);
echo (isset($error))?$OUTPUT->error_text($error):'';
echo $upload_form->display();
echo $OUTPUT->footer();
?>
