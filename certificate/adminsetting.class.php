<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Creates an upload form on the settings page
 *
 * @package    mod
 * @subpackage certificate
 * @copyright  Michael Avelar <michaela@moodlerooms.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->libdir.'/adminlib.php');

/**
* Class extends admin setting class to allow/process an uploaded file
**/
class mod_certificate_admin_setting_upload extends admin_setting_configtext {
    public function __construct($name, $visiblename, $description, $defaultsetting) {
        parent::__construct($name, $visiblename, $description, $defaultsetting, PARAM_RAW, 50);
    }
	
	var $nosave = true;

    function output_html($data, $query='') {
		global $DB,$PAGE;
        set_config('uploadimage',0,'certificate');
		$action = optional_param('action', '', PARAM_ALPHA);
		$id = optional_param('id', '',PARAM_INT);
		if($action == "delete"){
			$DB->delete_records("certificate_template", array("id"=>$id));
			$DB->delete_records("certificate_template_field", array("template_id"=>$id));
		}

		$templates = $DB->get_records_sql("SELECT id,template_name FROM {certificate_template}");

		$table = new html_table();
		$table->head = array('id', get_string('templatename','certificate'), get_string('action'));
		if(count($templates)<1)$table->data = array(array( "","No Data","")); else $table->data = array();

		foreach($templates as $template){
			$link = html_writer::link(new moodle_url($PAGE->url, array('id' => $template->id, 'action' => 'delete')), get_string('delete'));
			$link .=" ";
			$link .= html_writer::link(new moodle_url("/mod/certificate/edit_template.php", array('id' => $template->id)), get_string('edit'));
			$link .=" ";
			$link .= html_writer::link(new moodle_url("/mod/certificate/manage_field.php", array('id' => $template->id)), get_string('manage','certificate'));
			array_push($table->data,array($template->id, $template->template_name,$link));
				
		}

		$outputhtml = html_writer::table($table);
		$outputhtml .= html_writer::link(new moodle_url("/mod/certificate/create_template.php"),get_string('create_templete', 'certificate'), array('class' => 'button'));
        return $outputhtml;
    }
    public function write_setting($data) {
        return '';
    }
}

?>