<?php
require_once('../../grade/lib.php');
require_once('../../grade/querylib.php');
	
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from view.php
}
$template_fields = $DB->get_records_sql("SELECT id,z_index,type,value FROM {certificate_template_field} WHERE template_id='".$certificate->template_type."'");
$template = $DB->get_record_sql("SELECT orientation FROM {certificate_template} WHERE id='".$certificate->template_type."'");

foreach($template_fields as $template_field){
$fields_sort[$template_field->z_index.$template_field->id] = $template_field;
}
ksort($fields_sort);
$template_fields = $fields_sort;

$pdf = new PDF($template->orientation, 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetTitle($certificate->name);
$pdf->SetProtection(array('modify'));
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

foreach($template_fields as $template_field){
	$option = unserialize($template_field->value);
	
	if($template_field->type == "text"){
		$font_color = hex2rgb($option->font_color);
		$pdf->SetTextColor($font_color['r'], $font_color['g'], $font_color['b']);
		certificate_print_text($pdf, $certificate, $course, $certrecord, $option->x, $option->y, $option->text_leveling, $option->text_font, $option->font_style, $option->font_size, $option->text_area, $option->w, $option->text_types, $option->rotate);
	}elseif($template_field->type == "img"){
		certificate_print_image($pdf, $certificate, $option->image_name, $option->x, $option->y, $option->w, $option->h,$template_field->id);
	}

}


?>