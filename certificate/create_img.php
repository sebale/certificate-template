<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    mod
 * @subpackage certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require('../../config.php');
require_once($CFG->dirroot.'/mod/certificate/lib.php');
require_once($CFG->dirroot.'/mod/certificate/template_form.php');

$template_id = optional_param('id', 0, PARAM_INT);
require_login();

$context = context_system::instance();
require_capability('moodle/site:config', $context);

$title = get_string('create_img', 'certificate');

$PAGE->set_url('/mod/certificate/create_img.php');
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($SITE->fullname);
$PAGE->navbar->add('Certificate',new moodle_url("/admin/settings.php", array('section' => "modsettingcertificate")));
$PAGE->navbar->add(get_string('fields', 'certificate'),new moodle_url("/mod/certificate/manage_field.php", array('id' => $template_id)));
$PAGE->navbar->add($title);

$upload_form = new mod_certificate_create_template_field_img_form($CFG->wwwroot.'/mod/certificate/create_img.php?id='.$template_id);

if ($upload_form->is_cancelled()) {
    redirect(new moodle_url('/mod/certificate/manage_field.php', array('id' => $template_id)));
} else if ($data = $upload_form->get_data()) {
	
	$template = $DB->get_records_sql("SELECT id FROM {certificate_template_field} WHERE template_id=".$template_id." AND name='".$data->field_name."'");
  if(!empty($template)){
	$error =get_string('namenotempty','certificate') ;
  }else{
	$url = new moodle_url('/mod/certificate/manage_field.php', array("id" =>$data->template_id));
	
	$data->image_name = $upload_form->get_new_filename('image');			
			
	$insert = new stdclass;
	$insert->template_id = $data->template_id;
	$insert->name = $data->field_name;
	$insert->type = $data->field_type;
	$insert->z_index = $data->z;	
	unset ($data->z);
	unset ($data->template_id);
	unset ($data->field_name);
	unset ($data->field_type);
	unset ($data->submitbutton);	
	$insert->value = serialize($data);
	$insert->timecreate = time();
	
	$field_id = $DB->insert_record('certificate_template_field', $insert);	
	$storedfile = $upload_form->save_stored_file('image',"1",'mod_certificate', 'field_'.$field_id, "0");
	
	redirect($url);
  }
}

echo $OUTPUT->header();
require_once('jq_ui/head.php');
echo $OUTPUT->heading($title);
echo (isset($error))?$OUTPUT->error_text($error):'';
echo $upload_form->display();
echo $OUTPUT->footer();
?>
